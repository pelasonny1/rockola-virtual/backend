package main

import (
	"go-spotify/internal/handler"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/tracks", handler.HandleTracks)
	log.Println("Listening on http://localhost:8080/tracks")
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}
}
