module go-spotify

go 1.17

require (
	github.com/joho/godotenv v1.5.1
	golang.org/x/oauth2 v0.19.0
)
