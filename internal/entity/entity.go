package entity

// TrackInfo define la estructura para almacenar información detallada de la pista.
type TrackInfo struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Artist string `json:"artist"`
}

// PlaylistTracksResponse define la estructura de la respuesta JSON para las pistas de una playlist.
type PlaylistTracksResponse struct {
	Items []struct {
		Track struct {
			ID      string `json:"id"`
			Name    string `json:"name"`
			Artists []struct {
				Name string `json:"name"`
			} `json:"artists"`
		} `json:"track"`
	} `json:"items"`
}
