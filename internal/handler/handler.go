package handler

import (
	"encoding/json"
	spotify "go-spotify/internal/client"
	"net/http"
)

type Track struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Artist string `json:"artist"`
}

func HandleTracks(w http.ResponseWriter, r *http.Request) {
	playlistID := "3gr5iOCBePWeBTt9r6bMDI"

	tracks, err := spotify.GetPlaylistTracks(playlistID)
	if err != nil {
		http.Error(w, "Failed to fetch tracks: "+err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")

	if err := json.NewEncoder(w).Encode(tracks); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
