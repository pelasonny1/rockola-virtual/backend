package client

import (
	"context"
	"encoding/json"
	"fmt"
	"go-spotify/internal/entity"
	"io/ioutil"
	"os"

	"github.com/joho/godotenv"
	"golang.org/x/oauth2/clientcredentials"
)

// GetPlaylistTracks realiza una solicitud a la API de Spotify y retorna una lista de pistas.
func GetPlaylistTracks(playlistID string) ([]entity.TrackInfo, error) {
	err := godotenv.Load()
	if err != nil {
		return nil, fmt.Errorf("error loading .env file: %v", err)
	}

	config := &clientcredentials.Config{
		ClientID:     os.Getenv("SPOTIFY_CLIENT_ID"),
		ClientSecret: os.Getenv("SPOTIFY_CLIENT_SECRET"),
		TokenURL:     "https://accounts.spotify.com/api/token",
	}

	ctx := context.Background()
	client := config.Client(ctx)

	url := fmt.Sprintf("https://api.spotify.com/v1/playlists/%s/tracks?limit=10", playlistID)
	response, err := client.Get(url)
	if err != nil {
		return nil, fmt.Errorf("error making request to Spotify: %v", err)
	}
	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading response body: %v", err)
	}

	var playlistResp entity.PlaylistTracksResponse
	if err := json.Unmarshal(body, &playlistResp); err != nil {
		return nil, fmt.Errorf("error unmarshalling response: %v", err)
	}

	var tracks []entity.TrackInfo
	for _, item := range playlistResp.Items {
		artistNames := ""
		for _, artist := range item.Track.Artists {
			if artistNames != "" {
				artistNames += ", "
			}
			artistNames += artist.Name
		}
		tracks = append(tracks, entity.TrackInfo{
			ID:     item.Track.ID,
			Name:   item.Track.Name,
			Artist: artistNames,
		})
	}

	return tracks, nil
}
